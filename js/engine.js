$(function() {


// Фиксированная шапка 
// припрокрутке на втутренних
$(window).scroll(function(){

  if ($(window).width() > 1200) {
      var sticky = $('.inner-info .header'),
          scroll = $(window).scrollTop();
      if (scroll > 400) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});


$(window).resize(function() {
  if ($(window).width() <= 1200) {
      $('.inner-info .header').removeClass('header-fixed'); 
    }

});

// припрокрутке на главной
$(window).scroll(function(){

  if ($(window).width() > 1200) {
      var sticky = $('.home .header'),
          scroll = $(window).scrollTop();
      if (scroll > 800) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});
$(window).resize(function() {
  if ($(window).width() <= 1200) {
      $('.home .header').removeClass('header-fixed'); 
    }

});


// припрокрутке на странице категорий
$(window).scroll(function(){

  if ($(window).width() > 1200) {
      var sticky = $('.category .header'),
          scroll = $(window).scrollTop();
      if (scroll > 400) {
          sticky.addClass('header-fixed');
      } else {
          sticky.removeClass('header-fixed');
      };
    }

   
});
$(window).resize(function() {
  if ($(window).width() <= 1200) {
      $('.category .header').removeClass('header-fixed'); 
    }

});


// Прокрутка вверх при клике

var amountScrolled = 600;

$(window).scroll(function() {
  if ( $(window).scrollTop() > amountScrolled ) {
    $('a.back-to-top').fadeIn('200');
  } else {
    $('a.back-to-top').fadeOut('200');
  }
});
$('a.back-to-top').click(function() {
  $('html, body').animate({
    scrollTop: 0
  }, 700);
  return false;
});









// Выпадающее меню
if ($(window).width() >= 1199) {
   $('.dropdown').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   });

}else {
  $('.dropdown').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}



 // Стилизация селектов
$('select').styler();



// Счетчик для блока товара
$('.product-item__btn .btn').click(function() {
  $(this).parent().addClass('active');
})





// Banner slider
  $('.banner').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed: 4000,
      fade: true,
      arrows: false,
      dots: true,
      responsive: [{
        breakpoint: 1200,
        settings: {
            variableWidth: false,
        }
        
      }
    ]
     
  });





// reviews-slider
$('.reviews-slider').slick({
    infinite: true,
    arrows: true,
    slidesToShow: 6,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed:5000,
    
    responsive: [
    {
        breakpoint: 1365,
        settings: {
            slidesToShow: 5,
        }
        },
        {
        breakpoint: 1199,
        settings: {
            slidesToShow: 4,
        }

        },
        {
        breakpoint: 991,
        settings: {
            slidesToShow: 3,
        }

        },
        {
        breakpoint: 768,
        settings: {
            slidesToShow: 2,
        }

        },
        {
        breakpoint: 550,
        settings: {
            slidesToShow: 1,
        }

        },

    ]
   
});



// Фиксированный блок на странице Оформление заказа
if ($(window).width() > 992) {
      $('.airSticky').airStickyBlock();
};





// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
  autoFocus: false
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");

// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});
// Время доставки
$(function () {
    $('#timepicker').datetimepicker({
        locale: 'ru',
        format: 'HH:mm'

    });
});


// Показать\скрыть историю заказов
$('.history-item__header').click(function() {
  $(this).parent().toggleClass('active');

})




// Переключатель
$('.switch-btn').click(function (e, changeState) {
    if (changeState === undefined) {
        $(this).toggleClass('switch-on');
    }
    if ($(this).hasClass('switch-on')) {
        $(this).trigger('on.switch');
    } else {
        $(this).trigger('off.switch');
    }
});



// Новый адрес доставки

$('input:radio[name="delivery-group"]').change(
    function(){
        if ($('#delivery-new').is(':checked')) {
            $('.delivery-new').fadeIn(100)
        } else {
          $('.delivery-new').fadeOut(100)
        }
    });

// Добавить комментарий к адресу доставки
$('.delivery-new__address .switch-btn').click(function() {
  if($(this).hasClass('switch-on')){
    $('.delivery-new__address .form-field').fadeIn(100);
  }else{
    $('.delivery-new__address .form-field').fadeOut(100);
  }
})


// Begin of slider-range
$( "#slider-range" ).slider({
  range: 'min',
  min: 0,
  max: 500,
  value: 150,
  slide: function( event, ui ) {
    $( "#amount" ).text(ui.value);
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "value" ) );

// END of slider-range






// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});


// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})


$(window).scroll(function() {
  if ($(window).width() < 992) {
    if ( $(window).scrollTop() < 400 ) {
      $('.sidebar-mobile__btn').fadeIn('200');
    } else {
      $('.sidebar-mobile__btn').fadeOut('200');
    }
  }
});



// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}


// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();

})